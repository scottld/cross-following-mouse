import pygame as pg
pg.init()

screen = pg.display.set_mode((500,500))

def drawCross(surface, mouse_pos):
    #draw vertical line
    startPosVerticalLine = (mouse_position[0], 0)
    endPosVerticalLine = (mouse_position[0], screen.get_height())
    pg.draw.line(screen, (255,255,255), startPosVerticalLine, endPosVerticalLine)

    #draw vertical line
    startPosHorisontalLine = (0,mouse_position[1])
    endPosHorisontalLine = (screen.get_width(),mouse_position[1])
    pg.draw.line(screen, (255,255,255), startPosHorisontalLine, endPosHorisontalLine)


running = True
while running:
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
    

    screen.fill((0,0,0))
    
    mouse_position = pg.mouse.get_pos()
    drawCross(screen, mouse_position)
    pg.display.flip()